import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'models/alert.dart';

class AlertComponent{
    static AlertBase alertInstance= new AlertBase();
}

class AlertBase {

  showError(Alert alert) {
    return
      Future.delayed(Duration.zero, () => {Flushbar(
        title: alert.title,
        message: alert.message,
        backgroundColor: Colors.red,
        flushbarPosition: FlushbarPosition.TOP,
        duration: Duration(seconds: 3),
        boxShadows: [BoxShadow(
          color: Colors.green[800], offset: Offset(0.0, 2.0), blurRadius: 3.0,)
        ],
      ).show(alert.context)
  }
    );
  }

  showSuccess(Alert alert) {
    return Future.delayed(Duration.zero, () => {Flushbar(
      title: alert.title,
      message: alert.message,
      backgroundColor: Colors.green,
      flushbarPosition: FlushbarPosition.TOP,
      duration: Duration(seconds: 3),
      boxShadows: [BoxShadow(
        color: Colors.red[800], offset: Offset(0.0, 2.0), blurRadius: 3.0,)
      ],
    ).show(alert.context)
  }
    );
  }

  showToastMessage(BuildContext context, String message) {
    return Future.delayed(Duration.zero, () => {Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        textColor: Colors.white,
        fontSize: 16.0
    )
  }
    );
  }
}
import 'package:flutter/material.dart';

class Alert{
  BuildContext context;
  String title;
  String message;
  int durationInSeconds;
  Alert(this.context, this.title, this.message,durationInSeconds);

}